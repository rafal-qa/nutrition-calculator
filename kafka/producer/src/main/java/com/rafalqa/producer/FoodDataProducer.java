package com.rafalqa.producer;

import com.rafalqa.common.avro.models.FoodData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

import static java.lang.Thread.sleep;

@Slf4j
@Service
@RequiredArgsConstructor
public class FoodDataProducer implements CommandLineRunner {

    public static final String TOPIC_NAME = "foods";

    private final KafkaTemplate<String, FoodData> kafkaTemplate;

    private Random rand = new Random();

    @Bean
    NewTopic newTopic() {
        return TopicBuilder.name(TOPIC_NAME)
                .partitions(3)
                .replicas(1)
                .build();
    }

    @Override
    public void run(String... args) throws Exception {

        while (true) {
            FoodData value = generateValue();

            log.info("Producing to Kafka: " + value);
            kafkaTemplate.send(TOPIC_NAME, "amount", value);

            sleep(randomNumber(1000));
        }
    }

    private FoodData generateValue() {
        return FoodData.newBuilder()
                .setFdcId(randomNumber(4))
                .setAmount(randomNumber(100))
                .build();
    }

    private int randomNumber(int max) {
        return rand.nextInt(max) + 1;
    }
}
