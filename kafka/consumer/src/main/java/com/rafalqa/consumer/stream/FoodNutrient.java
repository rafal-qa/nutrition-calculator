package com.rafalqa.consumer.stream;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
public class FoodNutrient {
    @Id
    private Integer id;

    private Integer fdcId;
    private Integer nutrientId;
    private Double amount;
}
