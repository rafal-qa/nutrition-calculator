package com.rafalqa.consumer.stream;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodNutrientRepository extends JpaRepository<FoodNutrient, Integer> {
    FoodNutrient findByFdcIdAndNutrientId(Integer fdcId, Integer nutrientId);
}
