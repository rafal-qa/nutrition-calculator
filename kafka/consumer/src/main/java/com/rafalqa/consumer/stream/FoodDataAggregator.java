package com.rafalqa.consumer.stream;

import com.rafalqa.common.avro.models.FoodData;
import com.rafalqa.common.avro.models.FoodDataAggregated;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;

import static org.apache.kafka.streams.kstream.Suppressed.BufferConfig.unbounded;

@Service
@RequiredArgsConstructor
public class FoodDataAggregator {

    private final FoodNutrientRepository foodNutrientRepository;

    @Autowired
    public void calculateCountAndSum(StreamsBuilder streamsBuilder) {
        KStream<String, FoodData> stream = streamsBuilder.stream("foods");

        Duration windowSize = Duration.ofSeconds(30);
        Duration advanceSize = Duration.ofSeconds(5);
        TimeWindows hoppingWindow = TimeWindows.ofSizeWithNoGrace(windowSize).advanceBy(advanceSize);

        KTable<Windowed<String>, FoodDataAggregated> kTable = stream
                .groupByKey(Grouped.with(Serdes.String(), new SpecificAvroSerde<>()))
                .windowedBy(hoppingWindow)
                .aggregate(
                        () -> new FoodDataAggregated(0L, 0L),
                        (key, value, aggregate) -> {
                            aggregate.setRecordsCount(aggregate.getRecordsCount() + 1);
                            aggregate.setSumCalories(aggregate.getSumCalories() + calculateCalories(value));
                            return aggregate;
                        }
                )
                .suppress(Suppressed.untilWindowCloses(unbounded()));

        kTable.toStream((k, v) -> k.key()).to("foods-aggregated");
    }

    private Long calculateCalories(FoodData foodData) {
        final Integer caloriesNutrientId = 1;

        FoodNutrient foodNutrient = foodNutrientRepository
                .findByFdcIdAndNutrientId(foodData.getFdcId(), caloriesNutrientId);

        Double caloriesIn100Gram = foodNutrient.getAmount();
        double caloriesInOneGram = caloriesIn100Gram / 100;
        int productWeight = foodData.getAmount();

        double result = productWeight * caloriesInOneGram;

        return Math.round(result);
    }
}
