package com.rafalqa.consumer.aggregated;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodAggregatedLogRepository extends JpaRepository<FoodAggregatedLog, Integer> {

}
