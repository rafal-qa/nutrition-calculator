package com.rafalqa.consumer.aggregated;

import com.rafalqa.common.avro.models.FoodDataAggregated;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
@RequiredArgsConstructor
public class AggregatedDataConsumer {

    private final FoodAggregatedLogRepository foodAggregatedLogRepository;

    @KafkaListener(topics = "foods-aggregated", groupId = "nutrition-app-consumer")
    public void listen(ConsumerRecord<String, FoodDataAggregated> consumerRecord) {
        FoodDataAggregated data = consumerRecord.value();

        log.info("Consuming from Kafka: " + data);

        Date aggregationDate = new Date(consumerRecord.timestamp());

        FoodAggregatedLog foodLog = new FoodAggregatedLog();
        foodLog.setAggregationDate(aggregationDate);
        foodLog.setRecordsCount(data.getRecordsCount());
        foodLog.setSumCalories(data.getSumCalories());

        foodAggregatedLogRepository.saveAndFlush(foodLog);
    }
}
