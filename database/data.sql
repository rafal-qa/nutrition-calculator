--
-- PostgreSQL database dump
--

--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public."user" (id, email, password, enabled, display_name) VALUES (1, 'default@example.com', '$2a$10$GEdZ0TDokM7u4ofGjrD07ODvPNl4Ws6qRf.pEFuuo60KlbNtPecv6', true, 'Example User');


--
-- Data for Name: authority; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.authority (user_id, role) VALUES (1, 'USER');


--
-- Data for Name: food; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.food (fdc_id, description) VALUES (1, 'Apple');
INSERT INTO public.food (fdc_id, description) VALUES (2, 'Potato');
INSERT INTO public.food (fdc_id, description) VALUES (3, 'Banana');
INSERT INTO public.food (fdc_id, description) VALUES (4, 'Onion');


--
-- Data for Name: basket; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: nutrient; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.nutrient (id, name, unit_name) VALUES (1, 'Energy', 'kcal');


--
-- Data for Name: food_nutrient; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.food_nutrient (id, fdc_id, nutrient_id, amount) VALUES (1, 1, 1, 57);
INSERT INTO public.food_nutrient (id, fdc_id, nutrient_id, amount) VALUES (2, 2, 1, 87);
INSERT INTO public.food_nutrient (id, fdc_id, nutrient_id, amount) VALUES (3, 3, 1, 89);
INSERT INTO public.food_nutrient (id, fdc_id, nutrient_id, amount) VALUES (4, 4, 1, 41);


--
-- Data for Name: persistent_logins; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: verification_token; Type: TABLE DATA; Schema: public; Owner: -
--


--
-- PostgreSQL database dump complete
--

