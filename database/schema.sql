--
-- PostgreSQL database dump
--

--
-- Name: food_rows_count(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.food_rows_count() RETURNS bigint
    LANGUAGE sql
    AS $$
	SELECT count(*) FROM food;
$$;


--
-- Name: authority; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.authority (
    user_id integer NOT NULL,
    role character varying(10) NOT NULL
);


--
-- Name: basket; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.basket (
    id integer NOT NULL,
    user_id integer NOT NULL,
    fdc_id integer NOT NULL,
    amount integer NOT NULL
);


--
-- Name: basket_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.basket_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: basket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.basket_id_seq OWNED BY public.basket.id;


--
-- Name: food; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.food (
    fdc_id integer NOT NULL,
    description character varying NOT NULL
);


--
-- Name: food_aggregated_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.food_aggregated_log (
    id integer NOT NULL,
    aggregation_date timestamp without time zone NOT NULL,
    records_count bigint NOT NULL,
    sum_calories bigint NOT NULL
);


--
-- Name: food_aggregated_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.food_aggregated_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: food_aggregated_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.food_aggregated_log_id_seq OWNED BY public.food_aggregated_log.id;


--
-- Name: food_nutrient; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.food_nutrient (
    id integer NOT NULL,
    fdc_id integer NOT NULL,
    nutrient_id integer NOT NULL,
    amount double precision NOT NULL
);


--
-- Name: food_nutrient_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.food_nutrient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: food_nutrient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.food_nutrient_id_seq OWNED BY public.food_nutrient.id;


--
-- Name: nutrient; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.nutrient (
    id integer NOT NULL,
    name character varying NOT NULL,
    unit_name character varying NOT NULL
);


--
-- Name: persistent_logins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.persistent_logins (
    username character varying(64) NOT NULL,
    series character varying(64) NOT NULL,
    token character varying(64) NOT NULL,
    last_used timestamp without time zone NOT NULL
);


--
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying(100) NOT NULL,
    password character varying(100) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    display_name character varying(50) NOT NULL
);


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: verification_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.verification_token (
    id integer NOT NULL,
    user_id integer NOT NULL,
    token character varying(100) NOT NULL,
    token_type character varying(10) NOT NULL,
    expiry_date timestamp without time zone NOT NULL
);


--
-- Name: verification_token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.verification_token_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: verification_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.verification_token_id_seq OWNED BY public.verification_token.id;


--
-- Name: basket id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.basket ALTER COLUMN id SET DEFAULT nextval('public.basket_id_seq'::regclass);


--
-- Name: food_aggregated_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.food_aggregated_log ALTER COLUMN id SET DEFAULT nextval('public.food_aggregated_log_id_seq'::regclass);


--
-- Name: food_nutrient id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.food_nutrient ALTER COLUMN id SET DEFAULT nextval('public.food_nutrient_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Name: verification_token id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.verification_token ALTER COLUMN id SET DEFAULT nextval('public.verification_token_id_seq'::regclass);


--
-- Name: authority authority_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authority
    ADD CONSTRAINT authority_pk PRIMARY KEY (user_id, role);


--
-- Name: basket basket_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.basket
    ADD CONSTRAINT basket_pk PRIMARY KEY (id);


--
-- Name: food_aggregated_log food_aggregated_log_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.food_aggregated_log
    ADD CONSTRAINT food_aggregated_log_pk PRIMARY KEY (id);


--
-- Name: nutrient nutrients_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nutrient
    ADD CONSTRAINT nutrients_pk PRIMARY KEY (id);


--
-- Name: persistent_logins persistent_logins_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.persistent_logins
    ADD CONSTRAINT persistent_logins_pkey PRIMARY KEY (series);


--
-- Name: food_nutrient product_nutrient_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.food_nutrient
    ADD CONSTRAINT product_nutrient_pk PRIMARY KEY (id);


--
-- Name: food products_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.food
    ADD CONSTRAINT products_pk PRIMARY KEY (fdc_id);


--
-- Name: user user_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id);


--
-- Name: verification_token verification_token_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.verification_token
    ADD CONSTRAINT verification_token_pk PRIMARY KEY (id);


--
-- Name: basket_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX basket_user_id_index ON public.basket USING btree (user_id);


--
-- Name: product_nutrient_product_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX product_nutrient_product_id_idx ON public.food_nutrient USING btree (fdc_id);


--
-- Name: user_email_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_email_uindex ON public."user" USING btree (email);


--
-- Name: authority authority_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authority
    ADD CONSTRAINT authority_user_id_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: basket basket_fcd_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.basket
    ADD CONSTRAINT basket_fcd_id_fk FOREIGN KEY (fdc_id) REFERENCES public.food(fdc_id);


--
-- Name: basket basket_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.basket
    ADD CONSTRAINT basket_user_id_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: persistent_logins persistent_logins_username_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.persistent_logins
    ADD CONSTRAINT persistent_logins_username_fkey FOREIGN KEY (username) REFERENCES public."user"(email);


--
-- Name: food_nutrient product_nutrient_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.food_nutrient
    ADD CONSTRAINT product_nutrient_fk FOREIGN KEY (fdc_id) REFERENCES public.food(fdc_id);


--
-- Name: food_nutrient product_nutrient_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.food_nutrient
    ADD CONSTRAINT product_nutrient_fk_1 FOREIGN KEY (nutrient_id) REFERENCES public.nutrient(id);


--
-- Name: verification_token verification_token_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.verification_token
    ADD CONSTRAINT verification_token_user_id_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--

