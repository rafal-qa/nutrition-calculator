package com.rafalqa.nutrition.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rafalqa.nutrition.app.basket.Basket;
import com.rafalqa.nutrition.app.basket.BasketController;
import com.rafalqa.nutrition.app.basket.BasketService;
import com.rafalqa.nutrition.app.basket.BasketWithNutritionData;
import com.rafalqa.nutrition.data.nutrient.FoodNutrient;
import com.rafalqa.nutrition.security.AppUserDetails;
import com.rafalqa.nutrition.security.AppUserDetailsService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.sql.DataSource;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BasketController.class)
@Import(ControllerTestConfiguration.class)
public class BasketControllerTest {

    private static final Integer USER_ID = 3;

    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    AppUserDetails mockedUserDetails;

    @MockBean
    DataSource dataSource;

    @MockBean
    AppUserDetailsService appUserDetailsService;

    @MockBean
    BasketService basketService;

    @Captor
    ArgumentCaptor<Basket> basketCaptor;

    @Captor
    ArgumentCaptor<Integer> userIdCaptor;

    @Captor
    ArgumentCaptor<Integer> itemIdCaptor;

    @Captor
    ArgumentCaptor<Integer> itemAmountCaptor;

    @Test
    public void productsListWithNutritionDataShouldBeReturned() throws Exception {
        List<BasketWithNutritionData> basketItems = List.of(
                new BasketWithNutritionData(1, 101, 20, "Apple", List.of(
                        new FoodNutrient(1, 101, 1, 50.0)
                ))
        );
        Mockito.when(basketService.getListWithNutritionData(anyInt())).thenReturn(basketItems);

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/user/basket")
                .with(user(mockedUserDetails));

        MvcResult result = mvc.perform(request)
                .andExpect(status().isOk())
                .andReturn();

        Mockito.verify(basketService).getListWithNutritionData(userIdCaptor.capture());
        assertThat(userIdCaptor.getValue()).isEqualTo(USER_ID);

        assertThat(result.getResponse().getContentAsString())
                .isEqualTo(objectMapper.writeValueAsString(basketItems));
    }

    @Test
    public void itemShouldBeAddedToBasket() throws Exception {
        Basket addedItem = new Basket(1, USER_ID, 1, 20);
        Mockito.when(basketService.addItem(any())).thenReturn(addedItem);

        RequestBuilder request = MockMvcRequestBuilders
                .post("/api/user/basket")
                .with(user(mockedUserDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"fdcId\": 1,\"amount\": 20}");

        MvcResult result = mvc.perform(request)
                .andExpect(status().isCreated())
                .andReturn();

        Mockito.verify(basketService).addItem(basketCaptor.capture());
        Basket itemPassedToService = basketCaptor.getValue();

        assertThat(itemPassedToService.getId()).isNull();
        assertThat(itemPassedToService.getUserId()).isEqualTo(USER_ID);
        assertThat(itemPassedToService.getFdcId()).isEqualTo(1);
        assertThat(itemPassedToService.getAmount()).isEqualTo(20);

        assertThat(result.getResponse().getContentAsString())
                .isEqualTo(objectMapper.writeValueAsString(addedItem));
    }

    @Test
    public void wholeItemShouldBeUpdated() throws Exception {
        Basket updatedItem = new Basket(1, USER_ID, 1, 20);
        Mockito.when(basketService.updateItem(anyInt(), any())).thenReturn(updatedItem);
        Mockito.when(basketService.getUserIdOfItem(anyInt())).thenReturn(USER_ID);

        RequestBuilder request = MockMvcRequestBuilders
                .put("/api/user/basket/1")
                .with(user(mockedUserDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"fdcId\": 1,\"amount\": 20}");

        MvcResult result = mvc.perform(request)
                .andExpect(status().isOk())
                .andReturn();

        Mockito.verify(basketService).updateItem(itemIdCaptor.capture(), basketCaptor.capture());
        assertThat(itemIdCaptor.getValue()).isEqualTo(1);

        Basket itemPassedToService = basketCaptor.getValue();
        assertThat(itemPassedToService.getId()).isNull();
        assertThat(itemPassedToService.getUserId()).isNull();
        assertThat(itemPassedToService.getFdcId()).isEqualTo(1);
        assertThat(itemPassedToService.getAmount()).isEqualTo(20);

        assertThat(result.getResponse().getContentAsString())
                .isEqualTo(objectMapper.writeValueAsString(updatedItem));
    }

    @Test
    public void foodAmountShouldBeUpdated() throws Exception {
        Mockito.when(basketService.getUserIdOfItem(anyInt())).thenReturn(USER_ID);

        RequestBuilder request = MockMvcRequestBuilders
                .patch("/api/user/basket/1")
                .with(user(mockedUserDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\": 50}");

        mvc.perform(request)
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));

        Mockito.verify(basketService).updateFoodAmount(itemIdCaptor.capture(), itemAmountCaptor.capture());
        assertThat(itemIdCaptor.getValue()).isEqualTo(1);
        assertThat(itemAmountCaptor.getValue()).isEqualTo(50);
    }

    @Test
    public void fdcIdShouldNotBeUpdated() throws Exception {
        Mockito.when(basketService.getUserIdOfItem(anyInt())).thenReturn(USER_ID);

        RequestBuilder request = MockMvcRequestBuilders
                .patch("/api/user/basket/1")
                .with(user(mockedUserDetails))
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"fdcId\": 5}");

        mvc.perform(request).andExpect(status().isBadRequest());

        Mockito.verify(basketService, Mockito.never()).updateFoodAmount(anyInt(), anyInt());
    }

    @Test
    public void itemShouldBeDeleted() throws Exception {
        Mockito.when(basketService.getUserIdOfItem(anyInt())).thenReturn(USER_ID);

        RequestBuilder request = MockMvcRequestBuilders
                .delete("/api/user/basket/1")
                .with(user(mockedUserDetails));

        mvc.perform(request)
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));

        Mockito.verify(basketService).deleteItem(itemIdCaptor.capture());
        assertThat(itemIdCaptor.getValue()).isEqualTo(1);
    }
}
