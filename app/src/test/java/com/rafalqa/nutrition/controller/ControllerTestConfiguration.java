package com.rafalqa.nutrition.controller;

import com.rafalqa.nutrition.security.AppUserDetails;
import com.rafalqa.nutrition.user.authority.AuthorityRole;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;

@TestConfiguration
public class ControllerTestConfiguration {

    @Bean
    AppUserDetails mockedUserDetails() {
        return new AppUserDetails(
                3,
                "user",
                "pass",
                true,
                List.of(new SimpleGrantedAuthority(AuthorityRole.USER.name()))
        );
    }
}
