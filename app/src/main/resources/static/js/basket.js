$(document).ready(function() {

    const USER_BASKET_ENDPOINT = "/api/user/basket";

    // Display list

    const displayBasket = function() {
        $.get(USER_BASKET_ENDPOINT, function(result) {
            let $basketList = $("#basket ul");
            $("li", $basketList).remove();

            result.forEach(item => {
                let $basketElement = $($("#basket-element").html());
                $(".name", $basketElement).text(item.foodDescription);
                $(".remove", $basketElement).attr("data-item-id", item.id);
                $basketList.append($basketElement);
            });
        });
    }

    displayBasket();

    // Remove from list

    const removeBasketItem = function(itemId) {
        $.ajax({
            url: USER_BASKET_ENDPOINT + "/" + itemId,
            type: "DELETE",
            success: function() {
                $("#basket .remove").filter("[data-item-id=" + itemId + "]").parent().remove();
            }
        });
    }

    $("#basket").on("click", ".remove", function(e) {
        e.preventDefault();
        removeBasketItem($(this).attr("data-item-id"));
    });

    // Add to list

    const addProductToBasket = function(fdcId) {
        let basket = {
            fdcId: fdcId,
            amount: 1
        }
        $.ajax({
            url: USER_BASKET_ENDPOINT,
            type: "POST",
            data: JSON.stringify(basket),
            contentType: "application/json; charset=utf-8",
            success: function() {
                displayBasket();
            }
        });
    }

    $("#products").on("click", ".add", function(e) {
        e.preventDefault();
        addProductToBasket($(this).attr("data-fdc-id"));
    });

});
