$(document).ready(function() {

    const FOOD_LOG_ENDPOINT = "/api/stats/log";

    let latestId = 0;

    const getPreviousData = function() {
        $.get(FOOD_LOG_ENDPOINT, function(result) {
            result.forEach(item => {
                displayData(item);
                latestId = item.id;
            });
            watchForChanges();
        });
    }

    const watchForChanges = function() {
        setInterval(function(){
            getChangesSince();
        }, 4000);
    }

    const getChangesSince = function() {
        $.ajax({
            url: FOOD_LOG_ENDPOINT + "?since=" + latestId,
            type: "GET",
            timeout: 3000,
            success: function(result) {
                result.forEach(item => {
                    displayData(item);
                    latestId = item.id;
                });
            }
        });
    }

    const displayData = function(logData) {
        let $foodLog = $("#food-log ul");
        $foodLog.append(
            "<li>" +
            "ID: " + logData.id + " | " +
            "Date: " + logData.aggregationDate + " | " +
            "Records: " + logData.recordsCount + " | " +
            "Calories: " + logData.sumCalories +
            "</li>"
        );
    }

    getPreviousData();

});