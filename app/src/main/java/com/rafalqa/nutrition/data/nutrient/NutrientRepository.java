package com.rafalqa.nutrition.data.nutrient;

import org.springframework.data.jpa.repository.JpaRepository;

public interface NutrientRepository extends JpaRepository<Nutrient, Integer> {
}
