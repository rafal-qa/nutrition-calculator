package com.rafalqa.nutrition.data.food;

import java.util.List;

public interface FoodService {
    List<Food> getList();

    Long countFoods();
}
