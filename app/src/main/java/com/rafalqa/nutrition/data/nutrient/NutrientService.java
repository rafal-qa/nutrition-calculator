package com.rafalqa.nutrition.data.nutrient;

import java.util.List;

public interface NutrientService {
    List<Nutrient> getList();
}
