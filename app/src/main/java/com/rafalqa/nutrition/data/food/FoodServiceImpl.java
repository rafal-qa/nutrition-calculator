package com.rafalqa.nutrition.data.food;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FoodServiceImpl implements FoodService {

    private final FoodRepository foodRepository;

    @Override
    public List<Food> getList() {
        return foodRepository.findAll();
    }

    @Override
    public Long countFoods() {
        return foodRepository.getRowsCount();
    }
}
