package com.rafalqa.nutrition.data.nutrient;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FoodNutrientRepository extends JpaRepository<FoodNutrient, Integer> {
    @Query("select fn from FoodNutrient fn where fn.fdcId in :fdcIdList")
    List<FoodNutrient> findAllByFdcId(Iterable<Integer> fdcIdList);
}
