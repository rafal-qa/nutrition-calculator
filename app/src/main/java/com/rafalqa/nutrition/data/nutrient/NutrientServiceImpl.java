package com.rafalqa.nutrition.data.nutrient;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class NutrientServiceImpl implements NutrientService {

    private final NutrientRepository nutrientRepository;

    @Override
    public List<Nutrient> getList() {
        return nutrientRepository.findAll();
    }
}
