package com.rafalqa.nutrition.data.food;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;

public interface FoodRepository extends JpaRepository<Food, Integer> {
    @Procedure("food_rows_count")
    Long getRowsCount();
}
