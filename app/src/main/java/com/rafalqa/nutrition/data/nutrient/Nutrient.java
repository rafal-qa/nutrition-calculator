package com.rafalqa.nutrition.data.nutrient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Nutrient {
    @Id
    private Integer id;

    private String name;
    private String unitName;
}
