package com.rafalqa.nutrition.app.basket;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BasketRepository extends JpaRepository<Basket, Integer> {
    List<BasketWithNutritionData> findAllWithNutritionData(@Param("userId") Integer userId);

    @Query("select userId from Basket where id = :itemId")
    Integer findUserIdByItemId(Integer itemId);
}
