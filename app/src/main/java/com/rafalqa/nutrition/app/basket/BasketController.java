package com.rafalqa.nutrition.app.basket;

import com.rafalqa.nutrition.security.AppUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/user/basket")
@RequiredArgsConstructor
public class BasketController {

    private final BasketService basketService;

    @GetMapping
    public List<BasketWithNutritionData> list(@AuthenticationPrincipal AppUserDetails authUser) {
        return basketService.getListWithNutritionData(authUser.getUserId());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Basket create(@RequestBody Basket basket,
                         @AuthenticationPrincipal AppUserDetails authUser) {
        basket.setUserId(authUser.getUserId());

        return basketService.addItem(basket);
    }

    @PutMapping("{itemId}")
    public Basket update(@PathVariable Integer itemId,
                         @RequestBody Basket updatedBasket,
                         @AuthenticationPrincipal AppUserDetails authUser) {
        verifyItemIsOwnedByCurrentUser(authUser, itemId);

        return basketService.updateItem(itemId, updatedBasket);
    }

    @PatchMapping("{itemId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void partialUpdate(@PathVariable Integer itemId,
                              @RequestBody Basket updatedBasket,
                              @AuthenticationPrincipal AppUserDetails authUser) {
        verifyItemIsOwnedByCurrentUser(authUser, itemId);

        if (!updatedBasket.hasAmountOnly()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only amount update is possible");
        }

        basketService.updateFoodAmount(itemId, updatedBasket.getAmount());
    }

    @DeleteMapping("{itemId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Integer itemId,
                       @AuthenticationPrincipal AppUserDetails authUser) {
        verifyItemIsOwnedByCurrentUser(authUser, itemId);

        basketService.deleteItem(itemId);
    }

    private void verifyItemIsOwnedByCurrentUser(AppUserDetails currentUser, Integer itemId) {
        Integer currentUserId = currentUser.getUserId();
        Integer itemUserId = basketService.getUserIdOfItem(itemId);

        if (!Objects.equals(currentUserId, itemUserId)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                    "Basket item " + itemId + " is owned by another user");
        }
    }
}
