package com.rafalqa.nutrition.app.basket;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NamedQuery(name = "Basket.findAllWithNutritionData", query = Basket.WITH_NUTRITION_DATA_JPQL)
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Basket {

    public static final String WITH_NUTRITION_DATA_JPQL =
            "select new com.rafalqa.nutrition.app.basket.BasketWithNutritionData " +
            "(b.id, b.fdcId, b.amount, f.description) " +
            "from Basket b " +
            "join Food f " +
            "on b.fdcId = f.fdcId " +
            "where b.userId = :userId " +
            "order by b.id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer userId;
    private Integer fdcId;
    private Integer amount;

    public boolean hasAmountOnly() {
        return amount != null && id == null && fdcId == null;
    }
}
