package com.rafalqa.nutrition.app;

import com.rafalqa.nutrition.data.food.FoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/app")
@RequiredArgsConstructor
public class AppController {

    private final FoodService foodService;

    @GetMapping
    public String showAppPage(Model model) {
        model.addAttribute("foods", foodService.getList());

        return "app/index";
    }
}
