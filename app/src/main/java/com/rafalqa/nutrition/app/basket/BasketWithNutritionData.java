package com.rafalqa.nutrition.app.basket;

import com.rafalqa.nutrition.data.nutrient.FoodNutrient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class BasketWithNutritionData {
    private Integer id;
    private Integer fdcId;
    private Integer amount;
    private String foodDescription;
    private List<FoodNutrient> foodNutrients;

    public BasketWithNutritionData(Integer id, Integer fdcId, Integer amount, String foodDescription) {
        this.id = id;
        this.fdcId = fdcId;
        this.amount = amount;
        this.foodDescription = foodDescription;
    }
}
