package com.rafalqa.nutrition.app.basket;

import com.rafalqa.nutrition.data.nutrient.FoodNutrient;
import com.rafalqa.nutrition.data.nutrient.FoodNutrientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {

    private final BasketRepository basketRepository;
    private final FoodNutrientRepository foodNutrientRepository;

    @Override
    public List<BasketWithNutritionData> getListWithNutritionData(Integer userId) {
        List<Integer> fdcIdList = new ArrayList<>();
        List<BasketWithNutritionData> userList = basketRepository.findAllWithNutritionData(userId);

        userList.forEach(item -> fdcIdList.add(item.getFdcId()));

        List<FoodNutrient> allFoodNutrients = foodNutrientRepository.findAllByFdcId(fdcIdList);

        Map<Integer, List<FoodNutrient>> foodNutrientsByFdcId = allFoodNutrients.stream()
                .collect(groupingBy(FoodNutrient::getFdcId));

        userList.forEach(item -> {
            Integer fdcId = item.getFdcId();
            item.setFoodNutrients(foodNutrientsByFdcId.get(fdcId));
        });

        return userList;
    }

    @Override
    public Basket addItem(Basket basket) {
        return basketRepository.saveAndFlush(basket);
    }

    @Override
    public Basket updateItem(Integer itemId, Basket updatedBasket) {
        Basket basket = basketRepository.getById(itemId);
        BeanUtils.copyProperties(updatedBasket, basket, "id");
        return basketRepository.saveAndFlush(basket);
    }

    @Override
    public void updateFoodAmount(Integer itemId, Integer amount) {
        Basket basket = basketRepository.getById(itemId);
        basket.setAmount(amount);
        basketRepository.saveAndFlush(basket);
    }

    @Override
    public void deleteItem(Integer itemId) {
        basketRepository.deleteById(itemId);
    }

    @Override
    public Integer getUserIdOfItem(Integer itemId) {
        return basketRepository.findUserIdByItemId(itemId);
    }
}
