package com.rafalqa.nutrition.app.basket;

import java.util.List;

public interface BasketService {
    List<BasketWithNutritionData> getListWithNutritionData(Integer userId);

    Basket addItem(Basket basket);

    Basket updateItem(Integer itemId, Basket updatedBasket);

    void updateFoodAmount(Integer itemId, Integer amount);

    void deleteItem(Integer itemId);

    Integer getUserIdOfItem(Integer itemId);
}
