package com.rafalqa.nutrition.security;

import com.rafalqa.nutrition.user.User;
import com.rafalqa.nutrition.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AppUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }

        List<SimpleGrantedAuthority> grantedAuthorities =
                user.getAuthorities()
                        .stream()
                        .map(authority -> authority.getRole().name())
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());

        return new AppUserDetails(
                user.getId(),
                email,
                user.getPassword(),
                user.getEnabled(),
                grantedAuthorities
        );
    }
}
