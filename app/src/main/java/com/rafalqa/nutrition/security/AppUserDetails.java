package com.rafalqa.nutrition.security;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
public class AppUserDetails extends org.springframework.security.core.userdetails.User {

    private final Integer userId;

    public AppUserDetails(Integer userId, String username, String password, boolean enabled,
                          Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, true, true, true, authorities);
        this.userId = userId;
    }
}
