package com.rafalqa.nutrition.user;

import com.rafalqa.nutrition.user.event.OnNewUserEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final PasswordEncoder encoder;
    private final ApplicationEventPublisher eventPublisher;
    private final UserService userService;

    @GetMapping("login")
    public String loginForm() {
        return "user/login";
    }

    @GetMapping("new")
    public String createNewUserForm(@ModelAttribute("user") User user) {
        return "user/new";
    }

    @PostMapping("new")
    public String createNewUser(@Valid @ModelAttribute("user") User user, BindingResult result) {
        user.setPassword(encoder.encode(user.getPassword()));
        user = userService.createDefaultUser(user);

        eventPublisher.publishEvent(new OnNewUserEvent(user));

        return "redirect:new?created";
    }

    @GetMapping("confirm/{token}")
    public String confirmNewUser(@PathVariable String token) {
        userService.confirmNewUser(token);

        return "redirect:/user/login?confirmed";
    }
}
