package com.rafalqa.nutrition.user.authority;

import com.rafalqa.nutrition.user.User;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor @AllArgsConstructor
@EqualsAndHashCode
public class AuthorityId implements Serializable {
    private User user;
    private AuthorityRole role;
}
