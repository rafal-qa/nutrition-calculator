package com.rafalqa.nutrition.user.event;

import com.rafalqa.nutrition.user.User;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class OnNewUserEvent extends ApplicationEvent {
    private final User user;

    public OnNewUserEvent(User user) {
        super(user);
        this.user = user;
    }
}
