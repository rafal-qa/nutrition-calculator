package com.rafalqa.nutrition.user.verification;

import com.rafalqa.nutrition.user.User;

public interface VerificationTokenService {
    String generateNewToken();

    void saveAccountToken(User user, String token);

    void savePasswordToken(User user, String token);
}
