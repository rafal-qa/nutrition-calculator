package com.rafalqa.nutrition.user.verification;

public enum VerificationTokenType {
    ACCOUNT, PASSWORD
}
