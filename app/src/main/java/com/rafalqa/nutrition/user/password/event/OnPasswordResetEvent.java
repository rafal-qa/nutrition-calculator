package com.rafalqa.nutrition.user.password.event;

import com.rafalqa.nutrition.user.User;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class OnPasswordResetEvent extends ApplicationEvent {
    private final User user;

    public OnPasswordResetEvent(User user) {
        super(user);
        this.user = user;
    }
}
