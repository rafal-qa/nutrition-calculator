package com.rafalqa.nutrition.user;

import com.rafalqa.nutrition.user.authority.Authority;
import com.rafalqa.nutrition.user.authority.AuthorityRole;
import com.rafalqa.nutrition.user.verification.VerificationToken;
import com.rafalqa.nutrition.user.verification.VerificationTokenRepository;
import com.rafalqa.nutrition.user.verification.VerificationTokenType;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final PasswordEncoder encoder;
    private final UserRepository userRepository;
    private final VerificationTokenRepository verificationTokenRepository;

    @Override
    public User getUserById(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User createDefaultUser(User user) {
        user.setEnabled(false);

        Authority userAuthority = new Authority(user, AuthorityRole.USER);
        user.setAuthorities(Set.of(userAuthority));

        return userRepository.saveAndFlush(user);
    }

    @Override
    @Transactional
    public void confirmNewUser(String token) {
        VerificationToken verificationToken = verificationTokenRepository
                .findByTokenAndTokenType(token, VerificationTokenType.ACCOUNT);

        if (verificationToken.getExpiryDate().after(new Date())) {
            User user = getUserById(verificationToken.getUserId());
            user.setEnabled(true);

            userRepository.saveAndFlush(user);
            verificationTokenRepository.deleteById(verificationToken.getId());
        }
    }

    @Override
    @Transactional
    public void confirmNewPassword(String token, String password) {
        VerificationToken verificationToken = verificationTokenRepository
                .findByTokenAndTokenType(token, VerificationTokenType.PASSWORD);

        if (verificationToken.getExpiryDate().after(new Date())) {
            User user = getUserById(verificationToken.getUserId());
            user.setPassword(encoder.encode(password));

            userRepository.saveAndFlush(user);
            verificationTokenRepository.deleteById(verificationToken.getId());
        }
    }
}
