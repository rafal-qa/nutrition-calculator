package com.rafalqa.nutrition.user.authority;

import com.rafalqa.nutrition.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@IdClass(AuthorityId.class)
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class Authority {

    @Id
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Id
    @Enumerated(EnumType.STRING)
    private AuthorityRole role;
}
