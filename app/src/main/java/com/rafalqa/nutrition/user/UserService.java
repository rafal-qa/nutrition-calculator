package com.rafalqa.nutrition.user;

public interface UserService {
    User getUserById(Integer id);

    User getUserByEmail(String email);

    User createDefaultUser(User user);

    void confirmNewUser(String token);

    void confirmNewPassword(String token, String password);
}
