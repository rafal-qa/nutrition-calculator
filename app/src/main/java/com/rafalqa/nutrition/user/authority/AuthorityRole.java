package com.rafalqa.nutrition.user.authority;

public enum AuthorityRole {
    USER, ADMIN
}
