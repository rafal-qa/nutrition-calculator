package com.rafalqa.nutrition.user.verification;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter @Setter
public class VerificationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer userId;
    private String token;

    @Enumerated(EnumType.STRING)
    private VerificationTokenType tokenType;

    private Date expiryDate;
}
