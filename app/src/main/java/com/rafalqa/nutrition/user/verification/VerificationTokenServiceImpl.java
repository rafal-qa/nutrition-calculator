package com.rafalqa.nutrition.user.verification;

import com.rafalqa.nutrition.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class VerificationTokenServiceImpl implements VerificationTokenService {

    public static final int ONE_DAY_IN_MINUTES = 60 * 24;
    public static final int ONE_HOUR_IN_MINUTES = 60;

    private final VerificationTokenRepository verificationTokenRepository;

    @Override
    public String generateNewToken() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void saveAccountToken(User user, String token) {
        saveToken(
                user.getId(),
                token,
                VerificationTokenType.ACCOUNT,
                calculateExpiryDate(ONE_DAY_IN_MINUTES)
        );
    }

    @Override
    public void savePasswordToken(User user, String token) {
        saveToken(
                user.getId(),
                token,
                VerificationTokenType.PASSWORD,
                calculateExpiryDate(ONE_HOUR_IN_MINUTES)
        );
    }

    private void saveToken(Integer userId, String token, VerificationTokenType type, Date expiry) {
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUserId(userId);
        verificationToken.setToken(token);
        verificationToken.setTokenType(type);
        verificationToken.setExpiryDate(expiry);

        verificationTokenRepository.saveAndFlush(verificationToken);
    }

    private Date calculateExpiryDate(int expiration) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, expiration);
        return cal.getTime();
    }
}
