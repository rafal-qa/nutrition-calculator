package com.rafalqa.nutrition.user;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(Integer userId) {
        super("User [ID=" + userId + "] was not found");
    }
}
