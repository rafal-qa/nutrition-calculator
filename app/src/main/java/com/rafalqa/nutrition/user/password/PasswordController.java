package com.rafalqa.nutrition.user.password;

import com.rafalqa.nutrition.user.User;
import com.rafalqa.nutrition.user.UserService;
import com.rafalqa.nutrition.user.password.event.OnPasswordResetEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user/password")
@RequiredArgsConstructor
public class PasswordController {

    private final ApplicationEventPublisher eventPublisher;
    private final UserService userService;

    @GetMapping("reset")
    public String resetPasswordForm() {
        return "user/password/reset";
    }

    @PostMapping("reset")
    public String resetPassword(@RequestParam String email) {
        User user = userService.getUserByEmail(email);
        eventPublisher.publishEvent(new OnPasswordResetEvent(user));

        return "redirect:reset?sent";
    }

    @GetMapping("set/{token}")
    public String setNewPasswordForm(@PathVariable String token, Model model) {
        model.addAttribute("token", token);

        return "user/password/set";
    }

    @PostMapping("set")
    public String setNewPassword(@RequestParam String token, @RequestParam String password) {
        userService.confirmNewPassword(token, password);

        return "redirect:/user/login?password_changed";
    }
}
