package com.rafalqa.nutrition.user.event;

import com.rafalqa.nutrition.user.User;
import com.rafalqa.nutrition.user.verification.VerificationTokenService;
import com.rafalqa.nutrition.util.Mailer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NewUserListener implements ApplicationListener<OnNewUserEvent> {

    private final VerificationTokenService verificationTokenService;
    private final Mailer mailer;

    @Value("${app.host}")
    private String appHost;

    @Override
    public void onApplicationEvent(OnNewUserEvent event) {
        sendUserConfirmationToken(event);
    }

    private void sendUserConfirmationToken(OnNewUserEvent event) {
        User user = event.getUser();
        String token = verificationTokenService.generateNewToken();
        verificationTokenService.saveAccountToken(user, token);

        mailer.sendNotification(
                user.getEmail(),
                "Confirm account",
                appHost + "/user/confirm/" + token
        );
    }
}
