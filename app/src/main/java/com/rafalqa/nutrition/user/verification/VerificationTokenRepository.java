package com.rafalqa.nutrition.user.verification;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Integer> {
    VerificationToken findByTokenAndTokenType(String token, VerificationTokenType tokenType);
}
