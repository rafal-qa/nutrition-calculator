package com.rafalqa.nutrition.user.password.event;

import com.rafalqa.nutrition.user.User;
import com.rafalqa.nutrition.user.verification.VerificationTokenService;
import com.rafalqa.nutrition.util.Mailer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PasswordResetListener implements ApplicationListener<OnPasswordResetEvent> {

    private final VerificationTokenService verificationTokenService;
    private final Mailer mailer;

    @Value("${app.host}")
    private String appHost;

    @Override
    public void onApplicationEvent(OnPasswordResetEvent event) {
        sendPasswordResetToken(event);
    }

    private void sendPasswordResetToken(OnPasswordResetEvent event) {
        User user = event.getUser();
        String token = verificationTokenService.generateNewToken();
        verificationTokenService.savePasswordToken(user, token);

        mailer.sendNotification(
                user.getEmail(),
                "Reset password",
                appHost + "/user/password/set/" + token
        );
    }
}
