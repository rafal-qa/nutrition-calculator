package com.rafalqa.nutrition.user;

import com.rafalqa.nutrition.user.authority.Authority;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Entity
@Table(name = "\"user\"") // Because of error:
                          // org.postgresql.util.PSQLException: ERROR: syntax error at or near "user"
@Getter @Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Email
    private String email;

    @NotEmpty
    private String password;

    @NotEmpty
    private String displayName;

    private Boolean enabled;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Authority> authorities;
}
