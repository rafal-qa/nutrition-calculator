package com.rafalqa.nutrition.stats.log;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FoodAggregatedLogServiceImpl implements FoodAggregatedLogService {

    private final FoodAggregatedLogRepository foodAggregatedLogRepository;

    @Override
    public List<FoodAggregatedLog> getLatestRecords() {
        List<FoodAggregatedLog> result = foodAggregatedLogRepository.findTop10ByOrderByIdDesc();
        Collections.reverse(result);
        return result;
    }

    @Override
    public List<FoodAggregatedLog> getRecordsSinceId(Integer recordId) {
        List<FoodAggregatedLog> result = foodAggregatedLogRepository.findTop10ByIdIsGreaterThanOrderByIdDesc(recordId);
        Collections.reverse(result);
        return result;
    }
}
