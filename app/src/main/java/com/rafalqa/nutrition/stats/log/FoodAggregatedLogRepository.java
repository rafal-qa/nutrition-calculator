package com.rafalqa.nutrition.stats.log;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FoodAggregatedLogRepository extends JpaRepository<FoodAggregatedLog, Integer> {
    List<FoodAggregatedLog> findTop10ByOrderByIdDesc();

    List<FoodAggregatedLog> findTop10ByIdIsGreaterThanOrderByIdDesc(Integer id);
}
