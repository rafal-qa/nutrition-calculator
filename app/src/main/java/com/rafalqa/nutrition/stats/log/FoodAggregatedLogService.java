package com.rafalqa.nutrition.stats.log;

import java.util.List;

public interface FoodAggregatedLogService {
    List<FoodAggregatedLog> getLatestRecords();

    List<FoodAggregatedLog> getRecordsSinceId(Integer recordId);
}
