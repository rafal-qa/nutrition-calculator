package com.rafalqa.nutrition.stats;

import com.rafalqa.nutrition.stats.log.FoodAggregatedLog;
import com.rafalqa.nutrition.stats.log.FoodAggregatedLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/stats")
@RequiredArgsConstructor
public class StatsRestController {

    private final FoodAggregatedLogService foodAggregatedLogService;

    @GetMapping("log")
    public List<FoodAggregatedLog> getLogs(@RequestParam(required = false) Integer since) {
        if (since == null) {
            return foodAggregatedLogService.getLatestRecords();
        } else {
            return foodAggregatedLogService.getRecordsSinceId(since);
        }
    }
}
