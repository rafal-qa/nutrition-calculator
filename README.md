# Nutrition calculator

Java/Spring application

## Data source

The application was designed to use nutrition data from the [USDA database](https://fdc.nal.usda.gov).

It follows USDA's [API endpoints](https://fdc.nal.usda.gov/api-spec/fdc_api.html) 
and [database structure](https://fdc.nal.usda.gov/download-datasets.html) naming convention.

## Installation

* Create PostgreSQL database
* Create schema and import test data from `database` directory

## REST API endpoints

| Methods            | Endpoint | Description |
| ------------------ | -------- | ----------- |
| GET                | `/api/foods` | List of available food products |
| GET                | `/api/foods/count` | Number of all available products |
| GET                | `/api/nutrients` | List of all nutrients |
| GET, POST          | `/api/user/basket` | Food products with nutrition data added to user list |
| PUT, PATCH, DELETE | `/api/user/basket/{id}` | Management of user list item |

## Database

![Schema](database/schema.png)
